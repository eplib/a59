<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A59058">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The speech of Sir Charles Sidley in the House of Commons</title>
    <author>Sedley, Charles, Sir, 1639?-1701.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A59058 of text R8920 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S2404). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A59058</idno>
    <idno type="STC">Wing S2404</idno>
    <idno type="STC">ESTC R8920</idno>
    <idno type="EEBO-CITATION">12530552</idno>
    <idno type="OCLC">ocm 12530552</idno>
    <idno type="VID">62770</idno>
    <idno type="PROQUESTGOID">2264219621</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A59058)</note>
    <note>Transcribed from: (Early English Books Online ; image set 62770)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 293:11)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The speech of Sir Charles Sidley in the House of Commons</title>
      <author>Sedley, Charles, Sir, 1639?-1701.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for L.C. ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1691.</date>
     </publicationStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Tax protests and appeals -- Great Britain.</term>
     <term>Great Britain -- Politics and government -- 1689-1702.</term>
     <term>Broadsides -- England -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The speech of Sir Charles Sidley in the House of Commons.</ep:title>
    <ep:author>Sedley, Charles, Sir, </ep:author>
    <ep:publicationYear>1691</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>520</ep:wordCount>
    <ep:defectiveTokenCount>1</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>19.23</ep:defectRate>
    <ep:finalGrade>C</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 19.23 defects per 10,000 words puts this text in the C category of texts with between 10 and 35 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2002-08</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2002-09</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2002-10</date>
    <label>Chris Scherer</label>
        Sampled and proofread
      </change>
   <change>
    <date>2002-10</date>
    <label>Chris Scherer</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2002-12</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A59058-t">
  <body xml:id="A59058-e0">
   <div type="text" xml:id="A59058-e10">
    <pb facs="tcp:62770:1" rend="simple:additions" xml:id="A59058-001-a"/>
    <head xml:id="A59058-e20">
     <w lemma="the" pos="d" xml:id="A59058-001-a-0010">THE</w>
     <w lemma="speech" pos="n1" xml:id="A59058-001-a-0020">SPEECH</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-0030">OF</w>
     <w lemma="sir" pos="n1" xml:id="A59058-001-a-0040">Sir</w>
     <w lemma="CHARLES" pos="nn1" xml:id="A59058-001-a-0050">CHARLES</w>
     <w lemma="sidley" pos="nn1" xml:id="A59058-001-a-0060">SIDLEY</w>
     <w lemma="in" pos="acp" xml:id="A59058-001-a-0070">IN</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-0080">THE</w>
     <w lemma="house" pos="n1" xml:id="A59058-001-a-0090">House</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-0100">of</w>
     <w lemma="commons" pos="n2" xml:id="A59058-001-a-0110">Commons</w>
     <pc unit="sentence" xml:id="A59058-001-a-0120">.</pc>
    </head>
    <p xml:id="A59058-e30">
     <w lemma="we" pos="pns" xml:id="A59058-001-a-0130">WE</w>
     <w lemma="have" pos="vvb" xml:id="A59058-001-a-0140">have</w>
     <w lemma="provide" pos="vvn" xml:id="A59058-001-a-0150">provided</w>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-0160">for</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-0170">the</w>
     <w lemma="navy" pos="n1" xml:id="A59058-001-a-0180">Navy</w>
     <pc xml:id="A59058-001-a-0190">,</pc>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-0200">we</w>
     <w lemma="have" pos="vvb" xml:id="A59058-001-a-0210">have</w>
     <w lemma="provide" pos="vvn" xml:id="A59058-001-a-0220">provided</w>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-0230">for</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-0240">the</w>
     <w lemma="army" pos="n1" xml:id="A59058-001-a-0250">Army</w>
     <pc xml:id="A59058-001-a-0260">,</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-0270">and</w>
     <w lemma="now" pos="av" xml:id="A59058-001-a-0280">now</w>
     <w lemma="at" pos="acp" xml:id="A59058-001-a-0290">at</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-0300">the</w>
     <w lemma="latter" pos="j" xml:id="A59058-001-a-0310">latter</w>
     <w lemma="end" pos="n1" xml:id="A59058-001-a-0320">End</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-0330">of</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-0340">a</w>
     <w lemma="session" pos="n2" xml:id="A59058-001-a-0350">Sessions</w>
     <w lemma="here" pos="av" xml:id="A59058-001-a-0360">here</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-0370">is</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-0380">a</w>
     <w lemma="new" pos="j" xml:id="A59058-001-a-0390">new</w>
     <w lemma="reckon" pos="j-vg" xml:id="A59058-001-a-0400">Reckoning</w>
     <w lemma="bring" pos="vvn" xml:id="A59058-001-a-0410">brought</w>
     <w lemma="we" pos="pno" xml:id="A59058-001-a-0420">us</w>
     <pc xml:id="A59058-001-a-0430">,</pc>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-0440">we</w>
     <w lemma="must" pos="vmb" xml:id="A59058-001-a-0450">must</w>
     <w lemma="provide" pos="vvi" xml:id="A59058-001-a-0460">provide</w>
     <w lemma="likewise" pos="av" xml:id="A59058-001-a-0470">likewise</w>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-0480">for</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-0490">the</w>
     <w lemma="civil" pos="j" xml:id="A59058-001-a-0500">Civil</w>
     <w lemma="list" pos="n1" xml:id="A59058-001-a-0510">List</w>
     <pc xml:id="A59058-001-a-0520">:</pc>
     <w lemma="true" pos="av-j" xml:id="A59058-001-a-0530">Truly</w>
     <pc xml:id="A59058-001-a-0540">,</pc>
     <w lemma="mr." pos="ab" xml:id="A59058-001-a-0550">Mr.</w>
     <w lemma="speaker" pos="n1" xml:id="A59058-001-a-0560">Speaker</w>
     <pc xml:id="A59058-001-a-0570">,</pc>
     <w lemma="it" pos="pn" xml:id="A59058-001-a-0580">it</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-0590">is</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-0600">a</w>
     <w lemma="sad" pos="j" xml:id="A59058-001-a-0610">sad</w>
     <w lemma="reflection" pos="n1" xml:id="A59058-001-a-0620">Reflection</w>
     <w lemma="that" pos="cs" xml:id="A59058-001-a-0630">that</w>
     <w lemma="some" pos="d" xml:id="A59058-001-a-0640">some</w>
     <w lemma="man" pos="n2" xml:id="A59058-001-a-0650">Men</w>
     <w lemma="shall" pos="vmd" xml:id="A59058-001-a-0660">should</w>
     <w lemma="wallow" pos="vvi" xml:id="A59058-001-a-0670">wallow</w>
     <w lemma="in" pos="acp" xml:id="A59058-001-a-0680">in</w>
     <w lemma="wealth" pos="n1" xml:id="A59058-001-a-0690">Wealth</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-0700">and</w>
     <w lemma="place" pos="n2" xml:id="A59058-001-a-0710">Places</w>
     <pc xml:id="A59058-001-a-0720">,</pc>
     <w lemma="whilst" pos="cs" xml:id="A59058-001-a-0730">whilst</w>
     <w lemma="other" pos="pi2-d" xml:id="A59058-001-a-0740">others</w>
     <w lemma="pay" pos="vvb" xml:id="A59058-001-a-0750">pay</w>
     <w lemma="away" pos="av" xml:id="A59058-001-a-0760">away</w>
     <w lemma="in" pos="acp" xml:id="A59058-001-a-0770">in</w>
     <w lemma="tax" pos="n2" xml:id="A59058-001-a-0780">Taxes</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-0790">the</w>
     <w lemma="four" pos="ord" xml:id="A59058-001-a-0800">fourth</w>
     <w lemma="part" pos="n1" xml:id="A59058-001-a-0810">part</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-0820">of</w>
     <w lemma="their" pos="po" xml:id="A59058-001-a-0830">their</w>
     <w lemma="yearly" pos="av-j" xml:id="A59058-001-a-0840">Yearly</w>
     <w lemma="revenue" pos="n1" xml:id="A59058-001-a-0850">Revenue</w>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-0860">for</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-0870">the</w>
     <w lemma="support" pos="n1" xml:id="A59058-001-a-0880">Support</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-0890">of</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-0900">the</w>
     <w lemma="same" pos="d" xml:id="A59058-001-a-0910">same</w>
     <w lemma="government" pos="n1" xml:id="A59058-001-a-0920">Government</w>
     <pc xml:id="A59058-001-a-0930">;</pc>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-0940">we</w>
     <w lemma="be" pos="vvb" xml:id="A59058-001-a-0950">are</w>
     <w lemma="not" pos="xx" xml:id="A59058-001-a-0960">not</w>
     <w lemma="upon" pos="acp" xml:id="A59058-001-a-0970">upon</w>
     <w lemma="equal" pos="j" xml:id="A59058-001-a-0980">equal</w>
     <w lemma="term" pos="n2" xml:id="A59058-001-a-0990">Terms</w>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-1000">for</w>
     <w lemma="his" pos="po" xml:id="A59058-001-a-1010">his</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A59058-001-a-1020">Majesties</w>
     <w lemma="service" pos="n1" xml:id="A59058-001-a-1030">Service</w>
     <pc xml:id="A59058-001-a-1040">,</pc>
     <w lemma="the" pos="d" xml:id="A59058-001-a-1050">the</w>
     <w lemma="courtier" pos="n2" xml:id="A59058-001-a-1060">Courtiers</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-1070">and</w>
     <w lemma="great" pos="j" xml:id="A59058-001-a-1080">great</w>
     <w lemma="officer" pos="n2" xml:id="A59058-001-a-1090">Officers</w>
     <w lemma="charge" pos="vvb" xml:id="A59058-001-a-1100">Charge</w>
     <w lemma="as" pos="acp" xml:id="A59058-001-a-1110">as</w>
     <w lemma="it" pos="pn" xml:id="A59058-001-a-1120">it</w>
     <w lemma="be" pos="vvd" xml:id="A59058-001-a-1130">were</w>
     <w lemma="in" pos="acp" xml:id="A59058-001-a-1140">in</w>
     <w lemma="armour" pos="n1" xml:id="A59058-001-a-1150">Armour</w>
     <pc xml:id="A59058-001-a-1160">,</pc>
     <w lemma="they" pos="pns" xml:id="A59058-001-a-1170">they</w>
     <w lemma="feel" pos="vvb" xml:id="A59058-001-a-1180">feel</w>
     <w lemma="not" pos="xx" xml:id="A59058-001-a-1190">not</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-1200">the</w>
     <w lemma="tax" pos="n2" xml:id="A59058-001-a-1210">Taxes</w>
     <w lemma="by" pos="acp" xml:id="A59058-001-a-1220">by</w>
     <w lemma="reason" pos="n1" xml:id="A59058-001-a-1230">Reason</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-1240">of</w>
     <w lemma="their" pos="po" xml:id="A59058-001-a-1250">their</w>
     <w lemma="place" pos="n2" xml:id="A59058-001-a-1260">Places</w>
     <pc xml:id="A59058-001-a-1270">,</pc>
     <w lemma="while" pos="cs" xml:id="A59058-001-a-1280">while</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-1290">the</w>
     <w lemma="country" pos="n1" xml:id="A59058-001-a-1300">Country</w>
     <w lemma="gentleman" pos="n2" xml:id="A59058-001-a-1310">Gentlemen</w>
     <w lemma="be" pos="vvb" xml:id="A59058-001-a-1320">are</w>
     <w lemma="shoot" pos="vvn" xml:id="A59058-001-a-1330">shot</w>
     <w lemma="through" pos="acp" xml:id="A59058-001-a-1340">through</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-1350">and</w>
     <w lemma="through" pos="acp" xml:id="A59058-001-a-1360">through</w>
     <w lemma="with" pos="acp" xml:id="A59058-001-a-1370">with</w>
     <w lemma="they" pos="pno" xml:id="A59058-001-a-1380">them</w>
     <pc unit="sentence" xml:id="A59058-001-a-1390">.</pc>
    </p>
    <p xml:id="A59058-e40">
     <w lemma="the" pos="d" xml:id="A59058-001-a-1400">The</w>
     <w lemma="king" pos="n1" xml:id="A59058-001-a-1410">King</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-1420">is</w>
     <w lemma="please" pos="vvn" xml:id="A59058-001-a-1430">pleased</w>
     <w lemma="to" pos="prt" xml:id="A59058-001-a-1440">to</w>
     <w lemma="lay" pos="vvi" xml:id="A59058-001-a-1450">lay</w>
     <w lemma="his" pos="po" xml:id="A59058-001-a-1460">his</w>
     <w lemma="want" pos="n2" xml:id="A59058-001-a-1470">Wants</w>
     <w lemma="before" pos="acp" xml:id="A59058-001-a-1480">before</w>
     <w lemma="we" pos="pno" xml:id="A59058-001-a-1490">us</w>
     <pc xml:id="A59058-001-a-1500">,</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-1510">and</w>
     <w lemma="I" pos="pns" xml:id="A59058-001-a-1520">I</w>
     <w lemma="be" pos="vvm" xml:id="A59058-001-a-1530">am</w>
     <w lemma="confident" pos="j" xml:id="A59058-001-a-1540">confident</w>
     <w lemma="expect" pos="vvz" xml:id="A59058-001-a-1550">expects</w>
     <w lemma="our" pos="po" xml:id="A59058-001-a-1560">our</w>
     <w lemma="advice" pos="n1" xml:id="A59058-001-a-1570">Advice</w>
     <w lemma="upon" pos="acp" xml:id="A59058-001-a-1580">upon</w>
     <w lemma="it" pos="pn" xml:id="A59058-001-a-1590">it</w>
     <pc xml:id="A59058-001-a-1600">:</pc>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-1610">We</w>
     <w lemma="ought" pos="vmd" xml:id="A59058-001-a-1620">ought</w>
     <w lemma="therefore" pos="av" xml:id="A59058-001-a-1630">therefore</w>
     <w lemma="to" pos="prt" xml:id="A59058-001-a-1640">to</w>
     <w lemma="tell" pos="vvi" xml:id="A59058-001-a-1650">tell</w>
     <w lemma="he" pos="pno" xml:id="A59058-001-a-1660">him</w>
     <w lemma="what" pos="crq" xml:id="A59058-001-a-1670">what</w>
     <w lemma="pension" pos="n2" xml:id="A59058-001-a-1680">Pensions</w>
     <w lemma="be" pos="vvb" xml:id="A59058-001-a-1690">are</w>
     <w lemma="too" pos="av" xml:id="A59058-001-a-1700">too</w>
     <w lemma="great" pos="j" xml:id="A59058-001-a-1710">great</w>
     <pc xml:id="A59058-001-a-1720">,</pc>
     <w lemma="what" pos="crq" xml:id="A59058-001-a-1730">what</w>
     <w lemma="place" pos="n2" xml:id="A59058-001-a-1740">Places</w>
     <w lemma="may" pos="vmb" xml:id="A59058-001-a-1750">may</w>
     <w lemma="be" pos="vvi" xml:id="A59058-001-a-1760">be</w>
     <w lemma="extinguish" pos="vvn" reg="extinguished" xml:id="A59058-001-a-1770">extinguish'd</w>
     <w lemma="during" pos="acp" xml:id="A59058-001-a-1780">during</w>
     <w lemma="this" pos="d" xml:id="A59058-001-a-1790">this</w>
     <w lemma="time" pos="n1" xml:id="A59058-001-a-1800">Time</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-1810">of</w>
     <w lemma="war" pos="n1" xml:id="A59058-001-a-1820">War</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-1830">and</w>
     <w lemma="public" pos="j" reg="Public" xml:id="A59058-001-a-1840">Publick</w>
     <w lemma="calamity" pos="n2" xml:id="A59058-001-a-1850">Calamities</w>
     <pc unit="sentence" xml:id="A59058-001-a-1860">.</pc>
     <w lemma="his" pos="po" xml:id="A59058-001-a-1870">His</w>
     <w lemma="majesty" pos="n1" xml:id="A59058-001-a-1880">Majesty</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-1890">is</w>
     <w lemma="encompass" pos="vvn" reg="encompassed" xml:id="A59058-001-a-1900">encompass'd</w>
     <w lemma="with" pos="acp" xml:id="A59058-001-a-1910">with</w>
     <pc xml:id="A59058-001-a-1920">,</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-1930">and</w>
     <w lemma="see" pos="vvz" xml:id="A59058-001-a-1940">sees</w>
     <w lemma="nothing" pos="pix" xml:id="A59058-001-a-1950">nothing</w>
     <w lemma="but" pos="acp" xml:id="A59058-001-a-1960">but</w>
     <pc xml:id="A59058-001-a-1970">,</pc>
     <w lemma="plenty" pos="n1" xml:id="A59058-001-a-1980">Plenty</w>
     <pc xml:id="A59058-001-a-1990">,</pc>
     <w lemma="great" pos="j" xml:id="A59058-001-a-2000">great</w>
     <w lemma="table" pos="n2" xml:id="A59058-001-a-2010">Tables</w>
     <pc xml:id="A59058-001-a-2020">,</pc>
     <w lemma="coach" pos="n2" xml:id="A59058-001-a-2030">Coaches</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-2040">and</w>
     <w lemma="six" pos="crd" xml:id="A59058-001-a-2050">six</w>
     <w lemma="horse" pos="n2" xml:id="A59058-001-a-2060">Horses</w>
     <pc xml:id="A59058-001-a-2070">,</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-2080">and</w>
     <w lemma="all" pos="d" xml:id="A59058-001-a-2090">all</w>
     <w lemma="thing" pos="n2" xml:id="A59058-001-a-2100">things</w>
     <w lemma="suitable" pos="j" xml:id="A59058-001-a-2110">suitable</w>
     <pc xml:id="A59058-001-a-2120">;</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-2130">and</w>
     <w lemma="therefore" pos="av" xml:id="A59058-001-a-2140">therefore</w>
     <w lemma="can" pos="vmbx" xml:id="A59058-001-a-2150">cannot</w>
     <w lemma="imagine" pos="vvi" xml:id="A59058-001-a-2160">imagine</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-2170">the</w>
     <w lemma="want" pos="n1" xml:id="A59058-001-a-2180">Want</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-2190">and</w>
     <w lemma="misery" pos="n1" xml:id="A59058-001-a-2200">Misery</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-2210">of</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-2220">the</w>
     <w lemma="rest" pos="n1" xml:id="A59058-001-a-2230">rest</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-2240">of</w>
     <w lemma="his" pos="po" xml:id="A59058-001-a-2250">his</w>
     <w lemma="subject" pos="n2" xml:id="A59058-001-a-2260">Subjects</w>
     <pc xml:id="A59058-001-a-2270">:</pc>
     <w join="right" lemma="he" pos="pns" xml:id="A59058-001-a-2280">He</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A59058-001-a-2281">'s</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-2290">a</w>
     <w lemma="wise" pos="j" xml:id="A59058-001-a-2300">Wise</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-2310">and</w>
     <w lemma="virtuous" pos="j" xml:id="A59058-001-a-2320">Virtuous</w>
     <w lemma="prince" pos="n1" xml:id="A59058-001-a-2330">Prince</w>
     <pc xml:id="A59058-001-a-2340">,</pc>
     <w lemma="but" pos="acp" xml:id="A59058-001-a-2350">but</w>
     <w lemma="he" pos="pns" xml:id="A59058-001-a-2360">he</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-2370">is</w>
     <w lemma="but" pos="acp" xml:id="A59058-001-a-2380">but</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-2390">a</w>
     <w lemma="young" pos="j" xml:id="A59058-001-a-2400">Young</w>
     <w lemma="king" pos="n1" xml:id="A59058-001-a-2410">King</w>
     <pc xml:id="A59058-001-a-2420">,</pc>
     <w lemma="encompass" pos="vvn" xml:id="A59058-001-a-2430">encompassed</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-2440">and</w>
     <w lemma="hem" pos="vvn" reg="hemmed" xml:id="A59058-001-a-2450">hemm'd</w>
     <w lemma="in" pos="acp" xml:id="A59058-001-a-2460">in</w>
     <w lemma="among" pos="acp" xml:id="A59058-001-a-2470">among</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-2480">a</w>
     <w lemma="company" pos="n1" xml:id="A59058-001-a-2490">Company</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-2500">of</w>
     <w lemma="crafty" pos="j" xml:id="A59058-001-a-2510">Crafty</w>
     <w lemma="old" pos="j" xml:id="A59058-001-a-2520">old</w>
     <w lemma="courtier" pos="n2" xml:id="A59058-001-a-2530">Courtiers</w>
     <pc xml:id="A59058-001-a-2540">,</pc>
     <w lemma="to" pos="prt" xml:id="A59058-001-a-2550">to</w>
     <w lemma="say" pos="vvi" xml:id="A59058-001-a-2560">say</w>
     <w lemma="no" pos="dx" xml:id="A59058-001-a-2570">no</w>
     <w lemma="more" pos="dc" xml:id="A59058-001-a-2580">more</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-2590">of</w>
     <w lemma="they" pos="pno" xml:id="A59058-001-a-2600">them</w>
     <pc xml:id="A59058-001-a-2610">,</pc>
     <w lemma="with" pos="acp" xml:id="A59058-001-a-2620">with</w>
     <w lemma="place" pos="n2" xml:id="A59058-001-a-2630">Places</w>
     <pc xml:id="A59058-001-a-2640">,</pc>
     <w lemma="some" pos="d" xml:id="A59058-001-a-2650">some</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-2660">of</w>
     <w lemma="three" pos="crd" xml:id="A59058-001-a-2670">three</w>
     <w lemma="thousand" pos="crd" xml:id="A59058-001-a-2680">thousand</w>
     <pc xml:id="A59058-001-a-2690">,</pc>
     <w lemma="some" pos="d" xml:id="A59058-001-a-2700">some</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-2710">of</w>
     <w lemma="six" pos="crd" xml:id="A59058-001-a-2720">six</w>
     <pc xml:id="A59058-001-a-2730">,</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-2740">and</w>
     <w lemma="some" pos="d" xml:id="A59058-001-a-2750">some</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-2760">of</w>
     <w lemma="eleven" pos="crd" xml:id="A59058-001-a-2770">eleven</w>
     <w lemma="thousand" pos="crd" xml:id="A59058-001-a-2780">thousand</w>
     <pc xml:id="A59058-001-a-2790">:</pc>
     <w lemma="I" pos="pns" xml:id="A59058-001-a-2800">I</w>
     <w lemma="be" pos="vvm" xml:id="A59058-001-a-2810">am</w>
     <w lemma="tell" pos="vvn" xml:id="A59058-001-a-2820">told</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-2830">the</w>
     <w lemma="commissioner" pos="n2" xml:id="A59058-001-a-2840">Commissioners</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-2850">of</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-2860">the</w>
     <w lemma="treasury" pos="n1" xml:id="A59058-001-a-2870">Treasury</w>
     <w lemma="have" pos="vvb" xml:id="A59058-001-a-2880">have</w>
     <w lemma="three" pos="crd" xml:id="A59058-001-a-2890">three</w>
     <w lemma="thousand" pos="crd" xml:id="A59058-001-a-2900">thousand</w>
     <w lemma="pound" pos="n1" xml:id="A59058-001-a-2910">Pound</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-2920">a</w>
     <w lemma="year" pos="n1" xml:id="A59058-001-a-2930">Year</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-2940">a</w>
     <w lemma="piece" pos="n1" xml:id="A59058-001-a-2950">Piece</w>
     <pc xml:id="A59058-001-a-2960">:</pc>
     <w lemma="certain" pos="av-j" xml:id="A59058-001-a-2970">Certainly</w>
     <w lemma="such" pos="d" xml:id="A59058-001-a-2980">such</w>
     <w lemma="pension" pos="n2" xml:id="A59058-001-a-2990">Pensions</w>
     <pc xml:id="A59058-001-a-3000">,</pc>
     <w lemma="whatever" pos="crq" xml:id="A59058-001-a-3010">whatever</w>
     <w lemma="they" pos="pns" xml:id="A59058-001-a-3020">they</w>
     <w lemma="may" pos="vmb" xml:id="A59058-001-a-3030">may</w>
     <w lemma="have" pos="vvi" xml:id="A59058-001-a-3040">have</w>
     <w lemma="be" pos="vvn" xml:id="A59058-001-a-3050">been</w>
     <w lemma="former" pos="av-j" xml:id="A59058-001-a-3060">formerly</w>
     <pc xml:id="A59058-001-a-3070">,</pc>
     <w lemma="be" pos="vvb" xml:id="A59058-001-a-3080">are</w>
     <w lemma="much" pos="d" xml:id="A59058-001-a-3090">much</w>
     <w lemma="too" pos="av" xml:id="A59058-001-a-3100">too</w>
     <w lemma="great" pos="j" xml:id="A59058-001-a-3110">great</w>
     <w lemma="in" pos="acp" xml:id="A59058-001-a-3120">in</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-3130">the</w>
     <w lemma="present" pos="j" xml:id="A59058-001-a-3140">present</w>
     <w lemma="want" pos="n1" xml:id="A59058-001-a-3150">Want</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-3160">and</w>
     <w lemma="calamity" pos="n2" xml:id="A59058-001-a-3170">Calamities</w>
     <w lemma="that" pos="cs" xml:id="A59058-001-a-3180">that</w>
     <w lemma="reign" pos="vvz" xml:id="A59058-001-a-3190">reigns</w>
     <w lemma="every" pos="d" xml:id="A59058-001-a-3200">every</w>
     <w lemma="where" pos="crq" xml:id="A59058-001-a-3210">where</w>
     <w lemma="else" pos="av" xml:id="A59058-001-a-3220">else</w>
     <pc xml:id="A59058-001-a-3230">,</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-3240">and</w>
     <w lemma="it" pos="pn" xml:id="A59058-001-a-3250">it</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-3260">is</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-3270">a</w>
     <w lemma="general" pos="j" xml:id="A59058-001-a-3280">general</w>
     <w lemma="scandal" pos="n1" xml:id="A59058-001-a-3290">Scandal</w>
     <pc xml:id="A59058-001-a-3300">,</pc>
     <w lemma="that" pos="cs" xml:id="A59058-001-a-3310">That</w>
     <w lemma="a" pos="d" xml:id="A59058-001-a-3320">a</w>
     <w lemma="government" pos="n1" xml:id="A59058-001-a-3330">Government</w>
     <pc xml:id="A59058-001-a-3340">,</pc>
     <w lemma="so" pos="av" xml:id="A59058-001-a-3350">so</w>
     <w lemma="sick" pos="j" xml:id="A59058-001-a-3360">sick</w>
     <w lemma="at" pos="acp" xml:id="A59058-001-a-3370">at</w>
     <w lemma="heart" pos="n1" xml:id="A59058-001-a-3380">Heart</w>
     <w lemma="as" pos="acp" xml:id="A59058-001-a-3390">as</w>
     <w lemma="we" pos="png" xml:id="A59058-001-a-3400">ours</w>
     <pc xml:id="A59058-001-a-3410">,</pc>
     <w lemma="shall" pos="vmd" xml:id="A59058-001-a-3420">should</w>
     <w lemma="look" pos="vvi" xml:id="A59058-001-a-3430">look</w>
     <w lemma="so" pos="av" xml:id="A59058-001-a-3440">so</w>
     <w lemma="well" pos="av" xml:id="A59058-001-a-3450">well</w>
     <w lemma="in" pos="acp" xml:id="A59058-001-a-3460">in</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-3470">the</w>
     <w lemma="face" pos="n1" xml:id="A59058-001-a-3480">Face</w>
     <pc unit="sentence" xml:id="A59058-001-a-3490">.</pc>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-3500">We</w>
     <w lemma="must" pos="vmb" xml:id="A59058-001-a-3510">must</w>
     <w lemma="save" pos="vvi" xml:id="A59058-001-a-3520">save</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-3530">the</w>
     <w lemma="king" pos="n1" xml:id="A59058-001-a-3540">King</w>
     <w lemma="money" pos="n1" xml:id="A59058-001-a-3550">Money</w>
     <w lemma="where" pos="crq" xml:id="A59058-001-a-3560">where</w>
     <w lemma="ever" pos="av" xml:id="A59058-001-a-3570">ever</w>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-3580">we</w>
     <w lemma="can" pos="vmb" xml:id="A59058-001-a-3590">can</w>
     <pc xml:id="A59058-001-a-3600">,</pc>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-3610">for</w>
     <w lemma="I" pos="pns" xml:id="A59058-001-a-3620">I</w>
     <w lemma="be" pos="vvm" xml:id="A59058-001-a-3630">am</w>
     <w lemma="afraid" pos="j" xml:id="A59058-001-a-3640">afraid</w>
     <w lemma="our" pos="po" xml:id="A59058-001-a-3650">our</w>
     <w lemma="work" pos="n1" xml:id="A59058-001-a-3660">Work</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-3670">is</w>
     <w lemma="too" pos="av" xml:id="A59058-001-a-3680">too</w>
     <w lemma="big" pos="j" xml:id="A59058-001-a-3690">big</w>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-3700">for</w>
     <w lemma="our" pos="po" xml:id="A59058-001-a-3710">our</w>
     <w lemma="purse" pos="n2" xml:id="A59058-001-a-3720">Purses</w>
     <pc xml:id="A59058-001-a-3730">,</pc>
     <w lemma="if" pos="cs" xml:id="A59058-001-a-3740">if</w>
     <w lemma="thing" pos="n2" xml:id="A59058-001-a-3750">things</w>
     <w lemma="be" pos="vvb" xml:id="A59058-001-a-3760">be</w>
     <w lemma="not" pos="xx" xml:id="A59058-001-a-3770">not</w>
     <w lemma="manage" pos="vvn" reg="managed" xml:id="A59058-001-a-3780">mannaged</w>
     <w lemma="wi●…" pos="n1" xml:id="A59058-001-a-3790">wi●…</w>
     <w lemma="thrift" pos="n1" xml:id="A59058-001-a-3800">Thrift</w>
     <w lemma="imaginable" pos="j" xml:id="A59058-001-a-3810">imaginable</w>
     <pc unit="sentence" xml:id="A59058-001-a-3820">.</pc>
     <w lemma="when" pos="crq" xml:id="A59058-001-a-3830">When</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-3840">the</w>
     <w lemma="people" pos="n1" xml:id="A59058-001-a-3850">People</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-3860">of</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A59058-001-a-3870">England</w>
     <w lemma="see" pos="vvb" xml:id="A59058-001-a-3880">see</w>
     <w lemma="that" pos="cs" xml:id="A59058-001-a-3890">that</w>
     <w lemma="all" pos="d" xml:id="A59058-001-a-3900">all</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-3910">is</w>
     <w lemma="save" pos="vvn" xml:id="A59058-001-a-3920">saved</w>
     <w lemma="that" pos="cs" xml:id="A59058-001-a-3930">that</w>
     <w lemma="can" pos="vmb" xml:id="A59058-001-a-3940">can</w>
     <w lemma="be" pos="vvi" xml:id="A59058-001-a-3950">be</w>
     <w lemma="save" pos="vvn" xml:id="A59058-001-a-3960">saved</w>
     <pc xml:id="A59058-001-a-3970">,</pc>
     <w lemma="that" pos="cs" xml:id="A59058-001-a-3980">that</w>
     <w lemma="there" pos="av" xml:id="A59058-001-a-3990">there</w>
     <w lemma="be" pos="vvb" xml:id="A59058-001-a-4000">are</w>
     <w lemma="no" pos="dx" xml:id="A59058-001-a-4010">no</w>
     <w lemma="exorbitant" pos="j" xml:id="A59058-001-a-4020">exorbitant</w>
     <w lemma="pension" pos="n2" xml:id="A59058-001-a-4030">Pensions</w>
     <w lemma="nor" pos="ccx" xml:id="A59058-001-a-4040">nor</w>
     <w lemma="unnecessary" pos="j" xml:id="A59058-001-a-4050">unnecessary</w>
     <w lemma="salary" pos="n2" reg="Salaries" xml:id="A59058-001-a-4060">Sallaries</w>
     <pc xml:id="A59058-001-a-4070">,</pc>
     <w lemma="that" pos="cs" xml:id="A59058-001-a-4080">that</w>
     <w lemma="all" pos="d" xml:id="A59058-001-a-4090">all</w>
     <w lemma="be" pos="vvz" xml:id="A59058-001-a-4100">is</w>
     <w lemma="applve" pos="vvn" xml:id="A59058-001-a-4110">applved</w>
     <w lemma="to" pos="acp" xml:id="A59058-001-a-4120">to</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-4130">the</w>
     <w lemma="use" pos="vvb" xml:id="A59058-001-a-4140">Use</w>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-4150">for</w>
     <w lemma="which" pos="crq" xml:id="A59058-001-a-4160">which</w>
     <w lemma="it" pos="pn" xml:id="A59058-001-a-4170">it</w>
     <w lemma="be" pos="vvd" xml:id="A59058-001-a-4180">was</w>
     <w lemma="give" pos="vvn" xml:id="A59058-001-a-4190">given</w>
     <pc xml:id="A59058-001-a-4200">,</pc>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-4210">we</w>
     <w lemma="shall" pos="vmb" xml:id="A59058-001-a-4220">shall</w>
     <w lemma="give" pos="vvi" xml:id="A59058-001-a-4230">give</w>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-4240">and</w>
     <w lemma="they" pos="pns" xml:id="A59058-001-a-4250">they</w>
     <w lemma="will" pos="vmb" xml:id="A59058-001-a-4260">will</w>
     <w lemma="cheerful" pos="av-j" xml:id="A59058-001-a-4270">cheerfully</w>
     <w lemma="pay" pos="vvi" xml:id="A59058-001-a-4280">pay</w>
     <w lemma="whatever" pos="crq" xml:id="A59058-001-a-4290">whatever</w>
     <w lemma="his" pos="po" xml:id="A59058-001-a-4300">his</w>
     <w lemma="majesty" pos="n1" xml:id="A59058-001-a-4310">Majesty</w>
     <w lemma="can" pos="vmb" xml:id="A59058-001-a-4320">can</w>
     <w lemma="want" pos="vvi" xml:id="A59058-001-a-4330">want</w>
     <w lemma="to" pos="prt" xml:id="A59058-001-a-4340">to</w>
     <w lemma="secure" pos="vvi" xml:id="A59058-001-a-4350">secure</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-4360">the</w>
     <w lemma="protestant" pos="jnn" xml:id="A59058-001-a-4370">Protestant</w>
     <w lemma="religion" pos="n1" xml:id="A59058-001-a-4380">Religion</w>
     <pc xml:id="A59058-001-a-4390">,</pc>
     <w lemma="to" pos="prt" xml:id="A59058-001-a-4400">to</w>
     <w lemma="keep" pos="vvi" xml:id="A59058-001-a-4410">keep</w>
     <w lemma="out" pos="av" xml:id="A59058-001-a-4420">out</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-4430">the</w>
     <w lemma="king" pos="n1" xml:id="A59058-001-a-4440">King</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-4450">of</w>
     <w lemma="France" pos="nn1" rend="hi" xml:id="A59058-001-a-4460">France</w>
     <pc xml:id="A59058-001-a-4470">,</pc>
     <w lemma="I" pos="pns" xml:id="A59058-001-a-4480">I</w>
     <pc xml:id="A59058-001-a-4490">,</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-4500">and</w>
     <w lemma="king" pos="n1" xml:id="A59058-001-a-4510">King</w>
     <w lemma="James" pos="nn1" rend="hi" xml:id="A59058-001-a-4520">James</w>
     <w lemma="too" pos="av" xml:id="A59058-001-a-4530">too</w>
     <pc xml:id="A59058-001-a-4540">:</pc>
     <w lemma="who" pos="crq" xml:id="A59058-001-a-4550">Whom</w>
     <pc xml:id="A59058-001-a-4560">,</pc>
     <w lemma="by" pos="acp" xml:id="A59058-001-a-4570">by</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-4580">the</w>
     <w lemma="way" pos="n1" xml:id="A59058-001-a-4590">way</w>
     <pc xml:id="A59058-001-a-4600">,</pc>
     <w lemma="I" pos="pns" xml:id="A59058-001-a-4610">I</w>
     <w lemma="have" pos="vvb" xml:id="A59058-001-a-4620">have</w>
     <w lemma="not" pos="xx" xml:id="A59058-001-a-4630">not</w>
     <w lemma="hear" pos="vvn" xml:id="A59058-001-a-4640">heard</w>
     <w lemma="name" pos="vvn" xml:id="A59058-001-a-4650">named</w>
     <w lemma="this" pos="d" xml:id="A59058-001-a-4660">this</w>
     <w lemma="session" pos="n1" xml:id="A59058-001-a-4670">Session</w>
     <pc xml:id="A59058-001-a-4680">,</pc>
     <w lemma="whete" pos="jc" xml:id="A59058-001-a-4690">wheter</w>
     <w lemma="out" pos="av" xml:id="A59058-001-a-4700">out</w>
     <w lemma="of" pos="acp" xml:id="A59058-001-a-4710">of</w>
     <w lemma="fear" pos="n1" xml:id="A59058-001-a-4720">Fear</w>
     <pc xml:id="A59058-001-a-4730">,</pc>
     <w lemma="respect" pos="n1" xml:id="A59058-001-a-4740">Respect</w>
     <w lemma="or" pos="cc" xml:id="A59058-001-a-4750">or</w>
     <w lemma="discretion" pos="n1" xml:id="A59058-001-a-4760">Discretion</w>
     <w lemma="I" pos="pns" xml:id="A59058-001-a-4770">I</w>
     <w lemma="can" pos="vmbx" xml:id="A59058-001-a-4780">cannot</w>
     <w lemma="tell" pos="vvi" xml:id="A59058-001-a-4790">tell</w>
     <pc unit="sentence" xml:id="A59058-001-a-4800">.</pc>
     <w lemma="I" pos="pns" xml:id="A59058-001-a-4810">I</w>
     <w lemma="conclude" pos="vvb" xml:id="A59058-001-a-4820">conclude</w>
     <pc xml:id="A59058-001-a-4830">,</pc>
     <w lemma="mr." pos="ab" xml:id="A59058-001-a-4840">Mr.</w>
     <w lemma="speaker" pos="n1" xml:id="A59058-001-a-4850">Speaker</w>
     <pc xml:id="A59058-001-a-4860">,</pc>
     <w lemma="let" pos="vvb" xml:id="A59058-001-a-4870">Let</w>
     <w lemma="we" pos="pno" xml:id="A59058-001-a-4880">us</w>
     <w lemma="save" pos="vvi" xml:id="A59058-001-a-4890">save</w>
     <w lemma="the" pos="d" xml:id="A59058-001-a-4900">the</w>
     <w lemma="king" pos="n1" xml:id="A59058-001-a-4910">King</w>
     <w lemma="what" pos="crq" xml:id="A59058-001-a-4920">what</w>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-4930">we</w>
     <w lemma="can" pos="vmb" xml:id="A59058-001-a-4940">can</w>
     <pc xml:id="A59058-001-a-4950">,</pc>
     <w lemma="and" pos="cc" xml:id="A59058-001-a-4960">and</w>
     <w lemma="then" pos="av" xml:id="A59058-001-a-4970">then</w>
     <w lemma="let" pos="vvb" xml:id="A59058-001-a-4980">let</w>
     <w lemma="we" pos="pno" xml:id="A59058-001-a-4990">us</w>
     <w lemma="proceed" pos="vvi" xml:id="A59058-001-a-5000">proceed</w>
     <w lemma="to" pos="prt" xml:id="A59058-001-a-5010">to</w>
     <w lemma="give" pos="vvi" xml:id="A59058-001-a-5020">give</w>
     <w lemma="he" pos="pno" xml:id="A59058-001-a-5030">him</w>
     <w lemma="what" pos="crq" xml:id="A59058-001-a-5040">what</w>
     <w lemma="we" pos="pns" xml:id="A59058-001-a-5050">we</w>
     <w lemma="be" pos="vvb" xml:id="A59058-001-a-5060">are</w>
     <w lemma="able" pos="j" xml:id="A59058-001-a-5070">able</w>
     <pc unit="sentence" xml:id="A59058-001-a-5080">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A59058-e80">
   <div type="colophon" xml:id="A59058-e90">
    <p xml:id="A59058-e100">
     <w lemma="LONDON" pos="nn1" rend="hi" xml:id="A59058-001-a-5090">LONDON</w>
     <pc xml:id="A59058-001-a-5100">,</pc>
     <w lemma="print" pos="vvn" xml:id="A59058-001-a-5110">Printed</w>
     <w lemma="for" pos="acp" xml:id="A59058-001-a-5120">for</w>
     <hi xml:id="A59058-e120">
      <w lemma="l." pos="ab" xml:id="A59058-001-a-5130">L.</w>
      <w lemma="c." pos="ab" xml:id="A59058-001-a-5140">C.</w>
     </hi>
     <w lemma="near" pos="acp" xml:id="A59058-001-a-5150">near</w>
     <w lemma="Fleet-bridge" pos="nn1" rend="hi" xml:id="A59058-001-a-5160">Fleet-bridge</w>
     <pc unit="sentence" xml:id="A59058-001-a-5170">.</pc>
     <w lemma="1691." pos="crd" xml:id="A59058-001-a-5180">1691.</w>
     <pc unit="sentence" xml:id="A59058-001-a-5190"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
